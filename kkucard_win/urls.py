"""kkucard_win URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^card-print/(?P<front>.*)/(?P<back>.*)/$', views.card_print, name='card-print'),
    url(r'^card-write/(?P<ticket_id>.*)/$', views.card_write, name='card-write'),
    # url(r'^card-print/(?P<front>[0-9]+)/(?P<back>[0-9]+)/$'', views.card_print, name='card-print'),
]
