from django.http import HttpResponse

import codecs
import gzip
import re
import sys
import os
import errno
import urllib
import urllib2
import os
import json
import subprocess

def card_print(request, front, back):
    base_url = "http://10.109.5.80:8080/media/print/"
    url_front = base_url + front;
    url_back = base_url + back;
    print "Downloading card front image: " + url_front
    urllib.urlretrieve (url_front, "tmp-front.png")
    print "Downloading card back image: " + url_back
    urllib.urlretrieve (url_back, "tmp-back.png")

    print "Execute: PrintApp"
    p = subprocess.Popen('PrintApp.exe', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in p.stdout.readlines():
        print line,
    retval = p.wait()

    return HttpResponse("%s<br>%s"%(url_front, url_back))

def card_write(request, ticket_id):
    print "Execute: CardReader"
    p = subprocess.Popen(['CardReader.exe', ticket_id], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in p.stdout.readlines():
        print line,
    retval = p.wait()

    return HttpResponse("Ticket ID: %s"%(ticket_id))